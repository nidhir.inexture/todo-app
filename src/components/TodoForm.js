import React, {useState } from "react";
import "./Todo.css"

function TodoForm(props) {
    const [input,setInput] = useState("");
    const HandleSubmit = e => {
        e.preventDefault();
          props.onSubmit({
            id: Math.floor(Math.random() * 10000),
            text: input
          })
        setInput('');

    };
    const HandleChange = e => {
        setInput(e.target.value);
    };
    return(
            <form className="TodoForm" onSubmit={HandleSubmit}>
                <input
                type="text"
                placeholder="Enter ToDo"
                name="text"
                value={input}
                className="ToDoInput"
                onChange={HandleChange}
                />
                <button type="submit" className="TodoButton">Add ToDo</button>
            </form>
    )
}

export default TodoForm;
