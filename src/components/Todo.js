import React, {useState } from "react";
import TodoForm from "./TodoForm"; 
import TodoList from "./TodoList";
import "./Todo.css"

function Todo({ todos, completeTodo, removeTodo}){
    const [edit, setEdit] = useState({
        id: null,
        value: ''
    });

    return todos.map((todo, index) => (
        <div
          className={todo.isComplete ? 'todo-row complete' : 'todo-row'} 
          key={index}
         >

         <div key={todo.id} onClick={() => completeTodo(todo.id)}>
          {todo.text}
          </div>
        <button 
        onClick={() => removeTodo(todo.id)}
        className="delete"
        >Remove</button>
        </div> 
    ));
}

export default Todo;